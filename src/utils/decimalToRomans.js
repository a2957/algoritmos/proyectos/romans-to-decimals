import { Decimals } from "./reference";

const decimalToRomans = (decimal) => {
  let romans = "";
  while(decimal>0) {
    if(decimal>=1000){
      romans += Decimals[1000];
      decimal -= 1000;
      continue;
    }
    if(decimal>=900){
      romans += Decimals[900];
      decimal -= 900;
      continue;
    }
    if(decimal>=500){
      romans += Decimals[500];
      decimal -= 500;
      continue;
      
    }
    if(decimal>=400){
      romans += Decimals[400];
      decimal -= 400;
      continue;
      
    }
    if(decimal>=100){
      romans += Decimals[100];
      decimal -= 100;
      continue;
      
    }
    if(decimal>=90){
      romans += Decimals[90];
      decimal -= 90;
      continue;
      
    }
    if(decimal>=50){
      romans += Decimals[50];
      decimal -= 50;
      continue;
      
    }
    if(decimal>=40){
      romans += Decimals[40];
      decimal -= 40;
      continue;
    }
    if(decimal>=10){
      romans += Decimals[10];
      decimal -= 10;
      continue;
      
    }
    if(decimal>=9){
      romans += Decimals[9];
      decimal -= 9;
      continue;
    }
    if(decimal>=5){
      romans += Decimals[5];
      decimal -= 5;
      continue;
    }
    if(decimal>=4){
      romans += Decimals[4];
      decimal -= 4;
      continue;
    }
    if(decimal>=1){
      romans += Decimals[1];
      decimal -= 1;
      continue;
    }
  }
  return romans;
}

export default decimalToRomans;