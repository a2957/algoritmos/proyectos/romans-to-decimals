import {Romans} from './reference'

const romansToDecimals = (romans) => {
  let decimal = 0;
  for (let i = 0; i<romans.length; i++) {
    if(romans[i] === "C"){
      if(i === romans.length-1) {
        decimal += Romans["C"];
        continue;
      } else if( romans[i + 1] === "D"){
        decimal += Romans["CD"];
        i++;
        continue;
      } else if( romans[i + 1] === "M"){
        decimal += Romans["CM"];
        i++;
        continue;
      } else {
        decimal += Romans["C"];
        continue;
      }
    }
      
    else if(romans[i] === "X"){
      if(i === romans.length-1) {
        decimal += Romans["X"];
        continue;
      } else if( romans[i + 1] === "L"){
        decimal += Romans["XL"];
        i++;
        continue;
      } else if( romans[i + 1] === "C"){
        decimal += Romans["XC"];
        i++;
        continue;
      } else {
        decimal += Romans["X"];
        continue;
      }
    } 
    
    else if(romans[i] === "I"){
      if(i === romans.length-1) {
        decimal += Romans["I"];
        continue;
      } else if( romans[i + 1] === "V"){
        decimal += Romans["IV"];
        i++;
        continue;
      } else if( romans[i + 1] === "X"){
        decimal += Romans["IX"];
        i++;
        continue;
      } else {
        decimal += Romans["I"];
        continue;
      }
    } else {
      decimal += Romans[romans[i]];
    }
  }
  return decimal
}

export default romansToDecimals;