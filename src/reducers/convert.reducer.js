const initialState = '';

const changeResult = (state = initialState, action) => {
  switch (action.type) {
    case 'CONVERT_TO_DECIMAL':
      return action.payload;
    case 'CONVERT_TO_ROMAN':
      return action.payload;
    default:
      return state;
  }
};

export default changeResult;
