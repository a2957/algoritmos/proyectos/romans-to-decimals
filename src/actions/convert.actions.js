import decimalToRomans from "../utils/decimalToRomans"
import romansToDecimals from "../utils/romansToDecimals"

export const romanToDecimal = (roman) => {

  roman = romansToDecimals(roman)

  return {
    type: 'CONVERT_TO_DECIMAL',
    payload: roman
  }
}

export const decimalToRoman = (decimal) => {

  decimal = decimalToRomans(decimal)

  return {
    type: 'CONVERT_TO_ROMAN',
    payload: decimal
  }
}